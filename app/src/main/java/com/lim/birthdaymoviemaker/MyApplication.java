package com.lim.birthdaymoviemaker;

import android.app.Application;
import android.os.StrictMode;

import androidx.multidex.MultiDex;

/**
 * Created by Vishal Rathod
 */
public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        MultiDex.install(this);
    }
}
