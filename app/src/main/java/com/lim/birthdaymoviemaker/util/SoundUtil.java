package com.lim.birthdaymoviemaker.util;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;

/**
 * Created by Vishal Rathod
 */
public class SoundUtil {
    private static MediaPlayer mediaPlayer;

    public static void play(Context context, int resId) {
        if (mediaPlayer != null) {
            mediaPlayer.reset();
        }
        mediaPlayer = MediaPlayer.create(context, resId);
        mediaPlayer.start();
    }

    public static void play(Context context, Uri uri) {
        if (mediaPlayer != null) {
            mediaPlayer.reset();
        }
        mediaPlayer = MediaPlayer.create(context, uri);
        mediaPlayer.start();
    }

    public static void stop() {
        if (mediaPlayer != null) {
            mediaPlayer.reset();
        }
    }
}
