package com.lim.birthdaymoviemaker.util;

import android.graphics.Bitmap;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * Created by Vishal Rathod
 */
public class BitmapUtil {
    public static Bitmap bitmap;

    public static void saveBitmap(Bitmap bitmap, String path) {
        try {
            File filePath = new File(path);

            if(filePath.exists()){
                filePath.delete();
            }

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, new FileOutputStream(path, false));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
}
