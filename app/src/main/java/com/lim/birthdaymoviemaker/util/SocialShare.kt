package com.lim.birthdaymoviemaker.util

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.lim.birthdaymoviemaker.R
import java.io.File
import com.lim.birthdaymoviemaker.model.Share


class SocialShare {
    companion object {
        fun shareForAll(context: Context, file: File) {
            val intent = Intent(Intent.ACTION_SEND).apply {
                type = "image/*"
                putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file))
            }
            context.startActivity(intent)
        }

        fun socialShare(context: Context, file: File, pckg: String) {
            val intent = Intent(Intent.ACTION_SEND).apply {
                type = "image/*"
                if (pckg.isNotEmpty()) {
                    setPackage(pckg);
                }
                putExtra(Intent.EXTRA_TEXT, "")
                putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file))
            }

            try {
                context.startActivity(intent)
            } catch (ex: android.content.ActivityNotFoundException) {
                Toast.makeText(context, "App is not been installed in your device.", Toast.LENGTH_SHORT)
                        .show()
            }
        }

        fun sendMail(context: Context, file: File) {
            val intent = Intent(Intent.ACTION_SENDTO)
                    .apply {
                        type = "image/*"
                        data = Uri.parse("mailto:")
                        putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file))
                    }
            context.startActivity(Intent.createChooser(intent, "Share with email"))
        }

        fun prepareShareList(context: Context): List<Share> {
            val shareList = arrayListOf<Share>()

            shareList.apply {
                add(
                        Share(
                                1, ContextCompat.getDrawable(context, R.drawable.icon_share),
                                "Share", ""
                        )
                )
                add(
                        Share(
                                2, ContextCompat.getDrawable(context, R.drawable.icon_instagram),
                                "Instagram", "com.instagram.android"
                        )
                )
                add(
                        Share(
                                3, ContextCompat.getDrawable(context, R.drawable.icon_whatsapp),
                                "Whatsapp", "com.whatsapp"
                        )
                )
                add(
                        Share(
                                4, ContextCompat.getDrawable(context, R.drawable.icon_facebook),
                                "Facebook", "com.facebook.katana"
                        )
                )
                add(
                        Share(
                                5, ContextCompat.getDrawable(context, R.drawable.icon_messanger),
                                "Messanger", "com.facebook.orca"
                        )
                )
                add(
                        Share(
                                6, ContextCompat.getDrawable(context, R.drawable.icon_twitter),
                                "Twitter", "com.twitter.android"
                        )
                )
                add(
                        Share(
                                7, ContextCompat.getDrawable(context, R.drawable.icon_email),
                                "Email", ""
                        )
                )

            }

            return shareList
        }
    }
}