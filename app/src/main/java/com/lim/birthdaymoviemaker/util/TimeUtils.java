package com.lim.birthdaymoviemaker.util;

/**
 * Created by rgi-40 on 21/3/18.
 */

public class TimeUtils {
    public class MilliSeconds {
        public static final int ONE_HOUR = 3600000;
        public static final int ONE_MINUTE = 60000;
        public static final int ONE_SECOND = 1000;
    }
}
