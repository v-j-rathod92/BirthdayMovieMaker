package com.lim.birthdaymoviemaker.util;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Matrix.ScaleToFit;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.ExifInterface;
import android.net.Uri;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;


public class BitmapCompression {
    public static int calculateInSampleSize(Options options, int reqWidth, int reqHeight) {
        int height = options.outHeight;
        int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            int halfHeight = height / 2;
            int halfWidth = width / 2;
            while (halfHeight / inSampleSize > reqHeight && halfWidth / inSampleSize > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) {
        Options options = new Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static Bitmap decodeSampledBitmapFromAssets(Context a, String path, int reqWidth, int reqHeight) throws IOException {
        Options options = new Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(a.getAssets().open(path), new Rect(), options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeStream(a.getAssets().open(path), new Rect(), options);
    }

    public static Bitmap decodeFile(File f, int reqHeight, int reqWidth) {
        Bitmap bitmap = null;
        try {
            Options o = new Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);
            int scale = 1;
            while ((o.outWidth / scale) / 2 >= reqWidth && (o.outHeight / scale) / 2 >= reqHeight) {
                scale *= 2;
            }
            Options o2 = new Options();
            o2.inSampleSize = scale;
            bitmap = BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
        }
        return bitmap;
    }

    public static Bitmap adjustImageOrientation(File f, Bitmap image) {
        try {
            int rotate = 0;
            switch (new ExifInterface(f.getAbsolutePath()).getAttributeInt("Orientation", 1)) {
                case 3:
                    rotate = 180;
                    break;
                case 6:
                    rotate = 90;
                    break;
                case 8:
                    rotate = 270;
                    break;
            }
            if (rotate != 0) {
                int w = image.getWidth();
                int h = image.getHeight();
                Matrix mtx = new Matrix();
                mtx.preRotate((float) rotate);
                image = Bitmap.createBitmap(image, 0, 0, w, h, mtx, false);
            }
            return image;
        } catch (IOException e) {
            return null;
        }
    }

    public static Bitmap adjustImageOrientationUri(Context context, Uri f, Bitmap image) {
        Matrix matrix = new Matrix();
        Cursor cursor = context.getContentResolver().query(f, new String[]{"orientation"}, null, null, null);
        if (cursor.getCount() == 1) {
            cursor.moveToFirst();
            matrix.preRotate((float) cursor.getInt(0));
        }
        return Bitmap.createBitmap(image, 0, 0, image.getWidth(), image.getHeight(), matrix, false).copy(Config.ARGB_8888, true);
    }


    public static Matrix adjustImageOrientationMatrix(Context context, Uri f) {
        Matrix matrix = new Matrix();
        Cursor cursor = context.getContentResolver().query(f, new String[]{"orientation"}, null, null, null);
        if (cursor.getCount() == 1) {
            cursor.moveToFirst();
            matrix.preRotate((float) cursor.getInt(0));
        }
        return matrix;
    }

    public static Bitmap compressBitmap(Bitmap original) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        original.compress(CompressFormat.PNG, 100, out);
        return BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
    }

    public static Bitmap scaleCenterCrop(Bitmap source, int newHeight, int newWidth) {
        int sourceWidth = source.getWidth();
        int sourceHeight = source.getHeight();
        float scale = Math.max(((float) newWidth) / ((float) sourceWidth), ((float) newHeight) / ((float) sourceHeight));
        float scaledWidth = scale * ((float) sourceWidth);
        float scaledHeight = scale * ((float) sourceHeight);
        float left = (((float) newWidth) - scaledWidth) / 2.0f;
        float top = (((float) newHeight) - scaledHeight) / 2.0f;
        RectF targetRect = new RectF(left, top, left + scaledWidth, top + scaledHeight);
        Bitmap dest = Bitmap.createBitmap(newWidth, newHeight, source.getConfig());
        new Canvas(dest).drawBitmap(source, null, targetRect, null);
        return dest;
    }

    public static Bitmap getResizedBitmap(Bitmap roughBitmap, int width, int height) {
        Matrix m = new Matrix();
        m.setRectToRect(new RectF(0.0f, 0.0f, (float) roughBitmap.getWidth(), (float) roughBitmap.getHeight()), new RectF(0.0f, 0.0f, (float) width, (float) height), ScaleToFit.CENTER);
        float[] values = new float[9];
        m.getValues(values);
        return Bitmap.createScaledBitmap(roughBitmap, (int) (((float) roughBitmap.getWidth()) * values[0]), (int) (((float) roughBitmap.getHeight()) * values[4]), true);
    }
}
