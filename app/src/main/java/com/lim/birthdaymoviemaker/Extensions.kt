package com.lim.birthdaymoviemaker

import android.widget.FrameLayout
import com.universalvideoview.UniversalMediaController
import com.universalvideoview.UniversalVideoView

fun FrameLayout.setupVideoPlayer(universalVideoView: UniversalVideoView,
                                 universalMediaController: UniversalMediaController, videoPath: String) {

    universalVideoView.setVideoPath(videoPath)
    universalVideoView.setMediaController(universalMediaController)
    universalVideoView.setOnPreparedListener {
        universalVideoView.seekTo(100)
    }

    universalMediaController.show(2000)
}