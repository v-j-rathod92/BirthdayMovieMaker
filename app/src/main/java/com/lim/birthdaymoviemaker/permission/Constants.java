package com.lim.birthdaymoviemaker.permission;

/**
 * Created by caik on 2017/2/17.
 */

public class Constants {

  public static final String GRANT = "grant";
  public static final String DENIED = "denied";

}
