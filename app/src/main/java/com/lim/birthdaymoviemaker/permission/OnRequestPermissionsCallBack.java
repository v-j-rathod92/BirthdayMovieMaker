package com.lim.birthdaymoviemaker.permission;

/**
 * Created by Vishal Rathod
 */
public interface OnRequestPermissionsCallBack {
    void onGrant();

    void onDenied(String permission);

}
