package com.lim.birthdaymoviemaker.databinding

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lim.birthdaymoviemaker.adapter.ShareAdapter
import com.lim.birthdaymoviemaker.model.Share

@BindingAdapter("bind:setSharingList")
fun setSharingList(recyclerView: RecyclerView, sharingList: List<Share>?) {

    val context = recyclerView.context

    recyclerView.apply {
        layoutManager = LinearLayoutManager(
                context,
                LinearLayoutManager.HORIZONTAL, false
        )
        adapter = sharingList?.let { ShareAdapter(it, context) }
    }
}