package com.lim.birthdaymoviemaker.callback

import com.lim.birthdaymoviemaker.model.Share


interface OnShareItemClick {
    fun onItemClick(share: Share)
}