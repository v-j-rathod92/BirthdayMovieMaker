package com.lim.birthdaymoviemaker.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.media.MediaScannerConnection;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;

import com.arthenica.mobileffmpeg.Config;
import com.arthenica.mobileffmpeg.FFmpeg;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.google.android.gms.ads.AdView;
import com.lim.birthdaymoviemaker.R;
import com.lim.birthdaymoviemaker.custom.LockableScrollView;
import com.lim.birthdaymoviemaker.custom.MaterialPlayPauseButton;
import com.lim.birthdaymoviemaker.custom.sticker.StickerView;
import com.lim.birthdaymoviemaker.helper.AdHandler;
import com.lim.birthdaymoviemaker.model.Item;
import com.lim.birthdaymoviemaker.util.BitmapCompression;
import com.lim.birthdaymoviemaker.util.BitmapUtil;
import com.lim.birthdaymoviemaker.util.PathUtil;
import com.lim.birthdaymoviemaker.util.ScreenUtils;
import com.lim.birthdaymoviemaker.util.SoundUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.GPUImageContrastFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageEmbossFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageGammaFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageGrayscaleFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageHazeFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageHueFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageKuwaharaFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageLevelsFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageMonochromeFilter;
import jp.co.cyberagent.android.gpuimage.GPUImagePosterizeFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageSharpenFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageToonFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageView;
import jp.co.cyberagent.android.gpuimage.GPUImageVignetteFilter;

/**
 * Created by Vishal Rathod
 */
public class MakeMovieActivity extends BaseActivity implements GPUImage.ResponseListener<Bitmap> {
    public static final String SELECTED_SONG = "selected_song";
    private static final String TAG = "COMMAND";
    private static final int SELECT_SONG = 111;
    private static final int PICK_SONG = 112;
    private Context mContext;
    private String timeRe = "\\btime=\\b\\d\\d:\\d\\d:\\d\\d.\\d\\d";
    private double percen = 0.0d;
    private int last = 0;
    private float toatalSecond;
    private LinearLayout linearLayoutFrame, linearLayoutMusic, linearLayoutDuration,
            linearLayoutEffect, linearLayoutSticker;
    private View viewFrame, viewMusic, viewDuration, viewEffect, viewSticker;
    private SimpleDraweeView imgFrame;
    private GPUImageView imgPreview;
    private MaterialPlayPauseButton playPauseButton, playPauseButtonFinalVideo;
    private TextView txtStart, txtEnd, txtDurationMin;
    private LinearLayout llFrameList, llEffectList, llStickersList;
    private HorizontalScrollView hsvFrames, hsvEffect, hsvStickers;
    private RelativeLayout relativeLayoutDuration;
    private LinearLayout linearLayoutSong;
    private SeekBar seekBar, seekBarDuration;
    private ImageView imgCheck, imgHome;
    private List<Item> listFrames = new ArrayList<>();
    private List<Item> listStickers = new ArrayList<>();
    private List<Bitmap> listSelectedImages = new ArrayList<>();
    private String[] frameNames, stickersNames;
    private TextView txtDefaultMusic, txtAddMusic;

    private FrameLayout frameLayout;
    private AddFrameThumbs addFrameThumbs;
    private AddStickersThumbs addStickersThumbs;
    private int width, seekAddVal, seekIndex, seekVal, duration = 2;
    private boolean isPlay;
    private Timer timerForSeekBar, timerForSlideShow;
    private Toolbar toolbar;
    private ProgressDialog dialog;
    private String destinationPath;
    private List<Integer> listAudio = new ArrayList<>();
    private int currentSoundIndex;
    private ProgressBar progressBar;
    private boolean isDefaultSongSelected = true;
    private Uri customSongUri;
    private List<GPUImageFilter> listGPUFilters = new ArrayList<>();
    private List<String> listSelectedImagesPath = new ArrayList<>();
    private List<StickerView> listAddedStickers = new ArrayList<>();
    private StickerView stickerCurrentEdit;
    private final int START_EFFECT_ID_FROM = 100, START_STICKER_ID_FROM = 1000;
    private int count = 0, selectedFrameIndex = 0, selectedFilterIndex = 0;
    private AlertDialog dialogItem;
    private int seek = 100;
    private AdView bannerAd;
    private VideoView videoViewFinal;
    private AtomicBoolean isPlayFinal = new AtomicBoolean(false);
    private LinearLayout llShare;
    private LockableScrollView scrollView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_make_movie);

        init();
        setListener();
    }

    private void init() {
        mContext = this;
        AdHandler.getInstance(mContext);

        toolbar = findViewById(R.id.toolbar);

        linearLayoutFrame = findViewById(R.id.linearLayoutFrame);
        linearLayoutMusic = findViewById(R.id.linearLayoutMusic);
        linearLayoutDuration = findViewById(R.id.linearLayoutDuration);
        linearLayoutEffect = findViewById(R.id.linearLayoutEffect);
        linearLayoutSticker = findViewById(R.id.linearLayoutSticker);

        imgCheck = toolbar.findViewById(R.id.imgCheck);
        imgPreview = findViewById(R.id.imgPreview);
        frameLayout = findViewById(R.id.frameLayout);
        seekBar = findViewById(R.id.seekBar);
        seekBarDuration = findViewById(R.id.seekBarDuration);
        txtDurationMin = findViewById(R.id.txtDurationMin);
        txtStart = findViewById(R.id.txtStart);
        txtEnd = findViewById(R.id.txtEnd);
        imgFrame = findViewById(R.id.imgFrame);
        linearLayoutSong = findViewById(R.id.linearLayoutSong);
        relativeLayoutDuration = findViewById(R.id.relativeLayoutDuration);
        playPauseButton = findViewById(R.id.playPauseButton);
        llFrameList = findViewById(R.id.llFrameList);
        llEffectList = findViewById(R.id.llEffectList);
        llStickersList = findViewById(R.id.llStickersList);
        hsvFrames = findViewById(R.id.hsvFrames);
        hsvEffect = findViewById(R.id.hsvEffect);
        hsvStickers = findViewById(R.id.hsvStickers);
        viewFrame = findViewById(R.id.viewFrame);
        viewMusic = findViewById(R.id.viewMusic);
        viewDuration = findViewById(R.id.viewDuration);
        viewEffect = findViewById(R.id.viewEffect);
        viewSticker = findViewById(R.id.viewSticker);
        txtDefaultMusic = findViewById(R.id.txtDefaultMusic);
        txtAddMusic = findViewById(R.id.txtAddMusic);
        progressBar = findViewById(R.id.progressBar);
        bannerAd = findViewById(R.id.bannerAd);
        scrollView = findViewById(R.id.scrollView);

        ((TextView) toolbar.findViewById(R.id.txtTitle)).setText(getString(R.string.generate_video));

        listAudio.add(R.raw.birthday1);
        listAudio.add(R.raw.birthday2);
        listAudio.add(R.raw.birthday3);

        setVideoViewHeight();
        addFrames();
        loadEffects();
        getSelectedImageList();
        changeSeekBarColor();

        imgCheck.setVisibility(View.VISIBLE);
        seekBarDuration.setMax(10);

        setFinalDuration();

        loadAds();
    }

    private void loadAds() {
        AdHandler.loadBannerAd(bannerAd);
    }

    private void setVideoViewHeight() {
        frameLayout.post(() -> {
            width = frameLayout.getWidth();
            ViewGroup.LayoutParams params = frameLayout.getLayoutParams();
            params.height = width;
            frameLayout.setLayoutParams(params);
        });
    }

    private void addFrames() {
        addFrameThumbs = new AddFrameThumbs();
        addFrameThumbs.execute();
    }

    private void addStickers() {
        if (llStickersList.getChildCount() == 0) {
            addStickersThumbs = new AddStickersThumbs();
            addStickersThumbs.execute();
        }
    }

    private void addImageFilters() {
        if (llEffectList.getChildCount() == 0) {
            Bitmap thumbNail = ThumbnailUtils.extractThumbnail(listSelectedImages.get(0), 100, 100);
            new Thread(() -> GPUImage.getBitmapForMultipleFilters(thumbNail,
                    listGPUFilters, MakeMovieActivity.this)).start();
        }
    }

    private void getSelectedImageList() {
        try {
            listSelectedImagesPath = getIntent().getStringArrayListExtra(SelectPhotoActivity.SELECTED_IMAGES);
            for (String path : listSelectedImagesPath) {
                File file = new File(path);
                if (file.exists()) {
                    Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                    listSelectedImages.add(bitmap);
                }
            }

            imgPreview.setImage(listSelectedImages.get(0));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void changeSeekBarColor() {
        seekBar.getProgressDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
        seekBar.getThumb().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);

        seekBarDuration.getProgressDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
        seekBarDuration.getThumb().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
    }

    protected void showProgress() {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Processing Video... " + String.format("%.0f", new Object[]{Double.valueOf(percen)}) + "%");
        dialog.setCancelable(false);
        dialog.show();
    }

    private void setListener() {
        frameLayout.setOnClickListener(this);
        linearLayoutFrame.setOnClickListener(this);
        linearLayoutMusic.setOnClickListener(this);
        linearLayoutDuration.setOnClickListener(this);
        linearLayoutEffect.setOnClickListener(this);
        linearLayoutSticker.setOnClickListener(this);

        imgCheck.setOnClickListener(this);

        playPauseButton.setOnClickListener(this);

        viewFrame.setOnClickListener(this);
        viewMusic.setOnClickListener(this);
        viewDuration.setOnClickListener(this);
        viewEffect.setOnClickListener(this);
        viewSticker.setOnClickListener(this);

        txtDefaultMusic.setOnClickListener(this);
        txtAddMusic.setOnClickListener(this);

        seekBarDuration.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                txtDurationMin.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                duration = seekBar.getProgress();
                setFinalDuration();

                if (isPlay) {
                    if (timerForSlideShow != null) {
                        timerForSlideShow.cancel();
                    }

                    if (timerForSeekBar != null) {
                        timerForSeekBar.cancel();
                    }
                    playBtnClick();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        int id = v.getId();

        if (id >= 0 && id < listFrames.size()) {
            setImageFrame(id, v);
        } else if (id >= START_EFFECT_ID_FROM && id < listGPUFilters.size() + START_EFFECT_ID_FROM) {
            selectedFilterIndex = id - START_EFFECT_ID_FROM;
            setSelectedEffectThumb(v);
            imgPreview.setFilter(listGPUFilters.get(id - START_EFFECT_ID_FROM));
        } else if (id >= START_STICKER_ID_FROM && id < listStickers.size() + START_STICKER_ID_FROM) {
            try {
                int index = id - START_STICKER_ID_FROM;
                Bitmap bitmap = BitmapFactory.decodeStream(getAssets().open(stickersNames[index]));
                addStickerView(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            switch (id) {
                case R.id.frameLayout:
                    unSelectStickerIfAdded();
                    break;
                case R.id.imgCheck:
                    try {
                        new ProcessVideo().execute();
                    } catch (Exception e) {
                        e.printStackTrace();
                        new File(destinationPath).deleteOnExit();
                    }

                    break;
                case R.id.playPauseButton:
                    if (isPlay) {
                        timerForSeekBar.cancel();
                        timerForSlideShow.cancel();
                        seekBar.setProgress(0);
                        txtStart.setText("00:00");
                        playPauseButton.setToPlay();
                        SoundUtil.stop();
                        isPlay = false;
                    } else {
                        playBtnClick();
                    }
                    break;

                case R.id.linearLayoutFrame:
                    hsvFrames.setVisibility(View.VISIBLE);
                    linearLayoutSong.setVisibility(View.GONE);
                    relativeLayoutDuration.setVisibility(View.GONE);
                    hsvEffect.setVisibility(View.GONE);
                    hsvStickers.setVisibility(View.GONE);
                    handleOptionSelection(0);
                    break;
                case R.id.linearLayoutMusic:
                    hsvFrames.setVisibility(View.GONE);
                    linearLayoutSong.setVisibility(View.VISIBLE);
                    relativeLayoutDuration.setVisibility(View.GONE);
                    hsvEffect.setVisibility(View.GONE);
                    hsvStickers.setVisibility(View.GONE);
                    handleOptionSelection(1);
                    break;
                case R.id.linearLayoutDuration:
                    hsvFrames.setVisibility(View.GONE);
                    linearLayoutSong.setVisibility(View.GONE);
                    relativeLayoutDuration.setVisibility(View.VISIBLE);
                    hsvEffect.setVisibility(View.GONE);
                    hsvStickers.setVisibility(View.GONE);
                    handleOptionSelection(2);
                    break;
                case R.id.linearLayoutEffect:
                    hsvFrames.setVisibility(View.GONE);
                    linearLayoutSong.setVisibility(View.GONE);
                    relativeLayoutDuration.setVisibility(View.GONE);
                    hsvEffect.setVisibility(View.VISIBLE);
                    handleOptionSelection(3);
                    hsvStickers.setVisibility(View.GONE);
                    addImageFilters();
                    break;
                case R.id.linearLayoutSticker:
                    hsvFrames.setVisibility(View.GONE);
                    linearLayoutSong.setVisibility(View.GONE);
                    relativeLayoutDuration.setVisibility(View.GONE);
                    hsvEffect.setVisibility(View.GONE);
                    hsvStickers.setVisibility(View.VISIBLE);
                    handleOptionSelection(4);
                    addStickers();
                    break;

                case R.id.txtDefaultMusic:
                    SoundUtil.stop();
                    Intent intent = new Intent(mContext, AudioListActivity.class);
                    startActivityForResult(intent, SELECT_SONG);
                    overridePendingTransitionEnter();
                    break;

                case R.id.txtAddMusic:
                    SoundUtil.stop();
                    Intent intentAddAudio = new Intent();
                    intentAddAudio.setType("audio/*");
                    intentAddAudio.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(intentAddAudio, PICK_SONG);
                    overridePendingTransitionEnter();
                    break;

                case R.id.playPauseButtonFinalVideo:
                    boolean ans;
                    if (isPlayFinal.get()) {
                        playPauseButtonFinalVideo.setToPlay();
                        videoViewFinal.pause();
                        seek = videoViewFinal.getCurrentPosition();
                        ans = false;
                    } else {
                        playPauseButtonFinalVideo.setToPause();
                        videoViewFinal.seekTo(seek);
                        videoViewFinal.start();
                        ans = true;
                    }
                    isPlayFinal.set(ans);
                    break;

                case R.id.llShare:
                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    Uri screenshotUri = FileProvider.getUriForFile(this, "com.lim.birthdaymoviemaker.provider", new File("/storage/emulated/0/Birthday Movie Maker/Video/vid_1532932386323.mp4"));
                    sharingIntent.setType("video/*");
                    sharingIntent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
                    startActivity(Intent.createChooser(sharingIntent, "Share video using"));
                    break;
            }
        }
    }

    public String getFilePath() {
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator +
                getString(R.string.app_name) + File.separator + "TMP/");
        return mediaStorageDir.getAbsolutePath();
    }

    public String getVideOutputPath() {
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator +
                getString(R.string.app_name) + File.separator + "Video/");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            mediaStorageDir.mkdirs();
        }

        destinationPath = mediaStorageDir.getAbsolutePath() + "/vid_" + System.currentTimeMillis() + ".mp4";

        return destinationPath;
    }

    private class ProcessVideo extends AsyncTask<Void, Void, Void> {
        List<StickerView> listAddedStickers = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress();
            setCallback();

            for (int i = 0; i < frameLayout.getChildCount(); i++) {
                View child = frameLayout.getChildAt(i);

                if (child instanceof StickerView) {
                    StickerView stickerView = (StickerView) child;
                    stickerView.setInEdit(false);
                    listAddedStickers.add(stickerView);
                }
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            String audioPath = getSelectedAudio();
            applyImageDecoration(listAddedStickers);

            String imgPath = getFilePath();
            String outputPath = getVideOutputPath();

            //String[] command = new String[]{"-y", "-r", "1/" + duration, "-i", imgPath + "/frame_%5d.jpg", "-vcodec", "libx264", "-r", "2", "-pix_fmt", "yuv420p", "-preset", "ultrafast", outputPath};
            String[] command = new String[]{"-y", "-r", "1/" + duration, "-i", imgPath + "/frame_%5d.jpg", "-ss", "0", "-i", audioPath, "-map", "0:0", "-map", "1:0", "-vcodec", "libx264", "-r", "2", "-pix_fmt", "yuv420p", "-shortest", "-preset", "ultrafast", outputPath};
            FFmpeg.execute(command);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dialog.dismiss();
            deleteRecursive(new File(getFilePath()));
            MediaScannerConnection.scanFile(mContext, new String[]{destinationPath}, new String[]{"video/*"}, null);
            startActivity(new Intent(mContext, ShareVideoActivity.class)
                    .putExtra(ShareVideoActivity.Companion.getVIDEO_PATH(), destinationPath));
            MakeMovieActivity.this.finish();
        }
    }

    void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);

        fileOrDirectory.delete();
    }

    private int durationToprogtess(String input) {
        int progress = 0;
        Matcher matcher = Pattern.compile(this.timeRe).matcher(input);
        int MINUTE = 1 * 60;
        int HOUR = MINUTE * 60;
        if (TextUtils.isEmpty(input) || !input.contains("time=")) {
//            Log.e("time", "not contain time " + input);
            return this.last;
        }
        while (matcher.find()) {
            String time = matcher.group();
            String[] splitTime = time.substring(time.lastIndexOf(61) + 1).split(":");
            float hour = ((Float.valueOf(splitTime[0]).floatValue() * ((float) HOUR)) + (Float.valueOf(splitTime[1]).floatValue() * ((float) MINUTE))) + Float.valueOf(splitTime[2]).floatValue();
            this.toatalSecond = (float) seekVal / 1000;
            progress = (int) ((100.0f * hour) / this.toatalSecond);
            updateInMili(hour);
        }
        this.last = progress;
        return progress;
    }

    private void updateInMili(final float time) {
        new Handler(Looper.getMainLooper()).post(() -> {
            percen = (((double) time) * 100.0d) / ((double) toatalSecond);
            if (percen + 15.0d > 100.0d) {
                percen = 100.0d;
            } else {
                percen += 15.0d;
            }
            dialog.setMessage("Processing Video... " + String.format("%.0f", new Object[]{Double.valueOf(percen)}) + "%");
        });
    }

    private void playBtnClick() {
        try {
            if (listSelectedImages.size() > 0) {
                final int[] count = {0};
                seekVal = duration * listSelectedImages.size() * 1000;
                seekIndex = 0;
                seekAddVal = seekVal / (seekVal / 100);
                seekBar.setMax(seekVal);
                if (isPlay) {
                    seekIndex = 0;
                    seekBar.setProgress(0);
                }

                isPlay = true;
                playPauseButton.setToPause();

                timerForSeekBar = new Timer();
                timerForSlideShow = new Timer();
                if (isDefaultSongSelected) {
                    SoundUtil.play(mContext, listAudio.get(currentSoundIndex));
                } else {
                    SoundUtil.play(mContext, customSongUri);
                }

                timerForSlideShow.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        Log.e("slide", "ok");
                        runOnUiThread(() -> {
                            if (count[0] > 0 && count[0] < listSelectedImages.size()) {
                                imgPreview.setImage(listSelectedImages.get(count[0]));
                            }
                            if (count[0] == listSelectedImages.size()) {
                                timerForSlideShow.cancel();
                                imgPreview.setImage(listSelectedImages.get(0));
                                playPauseButton.setToPlay();
                            }

                            count[0]++;
                        });
                    }
                }, 100, duration * 1000);
                timerForSeekBar.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        try {
                            Log.e("seekbar", "ok");
                            runOnUiThread(() -> {
                                seekIndex += seekAddVal;
                                seekBar.setProgress(seekIndex);
                                txtStart.setText(formatTimeUnit(seekIndex));

                                if (seekIndex >= seekVal) {
                                    seekBar.setProgress(0);
                                    seekIndex = 0;
                                    txtStart.setText("00:00");
                                    playPauseButton.setToPlay();
                                    SoundUtil.stop();
                                    isPlay = false;
                                    if (timerForSeekBar != null) {
                                        timerForSeekBar.cancel();
                                    }
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, 100, 100);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleOptionSelection(int which) {
        View bottomOptionLine[] = new View[]{viewFrame, viewMusic, viewDuration, viewEffect, viewSticker};

        for (int i = 0; i < bottomOptionLine.length; i++) {
            if (i == which) {
                bottomOptionLine[i].setVisibility(View.VISIBLE);
            } else {
                bottomOptionLine[i].setVisibility(View.GONE);
            }
        }
    }

    private void setFinalDuration() {
        seekVal = duration * listSelectedImages.size() * 1000;
        txtEnd.setText(formatTimeUnit(seekVal));
    }

    private void showOpenFileDialog() {
        String title, message;

        title = "Video Created";
        message = "Your video has been stored in \"" + destinationPath + "\"";


        AlertDialog.Builder dialog = new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Play", (dialog1, which) -> openWith(new File(destinationPath)))
                .setNegativeButton("Cancel", (dialog12, which) -> dialog12.dismiss());
        dialog.show();
    }

    public void openWith(final File f) {
        try {
            Uri uri = PathUtil.fileToContentUri(this, f);
            if (uri == null) {
                uri = Uri.fromFile(f);
            }
            Intent intent = new Intent();
            intent.setAction(android.content.Intent.ACTION_VIEW);
            intent.setDataAndType(uri, "video/mp4");
            startActivity(Intent.createChooser(intent, "Play your Video"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SoundUtil.stop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_SONG && resultCode == RESULT_OK) {
            isDefaultSongSelected = true;
            currentSoundIndex = data.getIntExtra(SELECTED_SONG, 0);
        } else if (requestCode == PICK_SONG && resultCode == RESULT_OK) {
            isDefaultSongSelected = false;
            customSongUri = data.getData();
        }
    }

    private String saveAudioRawToStorage() {
        InputStream inputStream = getResources().openRawResource(listAudio.get(currentSoundIndex));
        String path = PathUtil.getAudioFilePath(this);
        try {
            OutputStream outputStream = new FileOutputStream(new File(path));
            copyFile(inputStream, outputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return path;
    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    @Override
    public void response(Bitmap item) {
        runOnUiThread(() -> {
            addBitmapToEffectLayout(item);
        });
    }

    private void addBitmapToEffectLayout(Bitmap item) {
        LinearLayout linearLayout = new LinearLayout(MakeMovieActivity.this);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        linearLayout.setGravity(Gravity.CENTER_VERTICAL);

        int padding = (int) getResources().getDimension(R.dimen.dp_3);
        linearLayout.setPadding(padding, padding, padding, padding);

        View child = getLayoutInflater().inflate(R.layout.layout_effect_thumb, null);
        LinearLayout llThumb = child.findViewById(R.id.llThumb);
        ImageView gpuImageView = child.findViewById(R.id.imgThumb);
        gpuImageView.setImageBitmap(item);

        llThumb.setId(START_EFFECT_ID_FROM + count);
        count += 1;
        llThumb.setOnClickListener(MakeMovieActivity.this);

        linearLayout.addView(child);
        llEffectList.addView(linearLayout);
    }

    private class AddFrameThumbs extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            listFrames.clear();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            frameNames = getNames("frame");
            for (String path : frameNames) {
                listFrames.add(new Item("asset:///" + path, true));
            }

            int length = listFrames.size();

            for (int i = 0; i < length; i++) {
                if (!addFrameThumbs.isCancelled()) {
                    int finalI = i;
                    runOnUiThread(() -> {
                        LinearLayout linearLayout = new LinearLayout(MakeMovieActivity.this);
                        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                                LinearLayout.LayoutParams.MATCH_PARENT));
                        linearLayout.setGravity(Gravity.CENTER_VERTICAL);

                        int padding = (int) getResources().getDimension(R.dimen.dp_3);
                        linearLayout.setPadding(padding, padding, padding, padding);

                        View child = getLayoutInflater().inflate(R.layout.layout_thumb, null);
                        LinearLayout llThumb = child.findViewById(R.id.llThumb);
                        SimpleDraweeView sdvThumb = child.findViewById(R.id.imgThumb);

                        Log.e("path", Uri.parse(listFrames.get(finalI).path).toString());
                        int size = ScreenUtils.convertDIPToPixels(MakeMovieActivity.this,
                                getResources().getDimension(R.dimen.dp_50));
                        final ImageRequest imageRequest =
                                ImageRequestBuilder.newBuilderWithSource(Uri.parse(listFrames.get(finalI).path))
                                        .setResizeOptions(new ResizeOptions(size, size))
                                        .build();

                        sdvThumb.setImageRequest(imageRequest);
                        llThumb.setId(finalI);
                        llThumb.setOnClickListener(MakeMovieActivity.this);

                        linearLayout.addView(child);
                        llFrameList.addView(linearLayout);


                    });
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            progressBar.setVisibility(View.GONE);
        }
    }

    private class AddStickersThumbs extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            listStickers.clear();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            stickersNames = getStickerNames("stickers/balloons");
            for (String path : stickersNames) {
                listStickers.add(new Item("asset:///" + path, true));
            }

            int length = listStickers.size();

            for (int i = 0; i < length; i++) {
                if (!addStickersThumbs.isCancelled()) {
                    int finalI = i;
                    runOnUiThread(() -> {
                        LinearLayout linearLayout = new LinearLayout(MakeMovieActivity.this);
                        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                                LinearLayout.LayoutParams.MATCH_PARENT));
                        linearLayout.setGravity(Gravity.CENTER_VERTICAL);

                        int padding = (int) getResources().getDimension(R.dimen.dp_3);
                        linearLayout.setPadding(padding, padding, padding, padding);

                        View child = getLayoutInflater().inflate(R.layout.layout_thumb, null);
                        LinearLayout llThumb = child.findViewById(R.id.llThumb);
                        SimpleDraweeView sdvThumb = child.findViewById(R.id.imgThumb);

                        int size = ScreenUtils.convertDIPToPixels(MakeMovieActivity.this,
                                getResources().getDimension(R.dimen.dp_50));
                        final ImageRequest imageRequest =
                                ImageRequestBuilder.newBuilderWithSource(Uri.parse(listStickers.get(finalI).path))
                                        .setResizeOptions(new ResizeOptions(size, size))
                                        .build();

                        sdvThumb.setImageRequest(imageRequest);
                        llThumb.setId(START_STICKER_ID_FROM + finalI);
                        llThumb.setOnClickListener(MakeMovieActivity.this);

                        linearLayout.addView(child);
                        llStickersList.addView(linearLayout);
                    });
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            progressBar.setVisibility(View.GONE);
        }
    }

    private void loadEffects() {
        listGPUFilters.add(new GPUImageFilter());
        listGPUFilters.add(new GPUImageContrastFilter(2));
        listGPUFilters.add(new GPUImageHueFilter(90));
        listGPUFilters.add(new GPUImageGammaFilter(2));
        listGPUFilters.add(new GPUImageGrayscaleFilter());

        GPUImageSharpenFilter sharpness = new GPUImageSharpenFilter();
        sharpness.setSharpness(2.0f);
        listGPUFilters.add(sharpness);

        listGPUFilters.add(new GPUImageEmbossFilter());
        listGPUFilters.add(new GPUImagePosterizeFilter());
        listGPUFilters.add(new GPUImageMonochromeFilter(1, new float[]{0.6f, 0.45f, 0.3f, 1.0f}));

        PointF centerPoint = new PointF();
        centerPoint.x = 0.5f;
        centerPoint.y = 0.5f;
        listGPUFilters.add(new GPUImageVignetteFilter(centerPoint, new float[]{0.0f, 0.0f, 0.0f}, 0.3f, 0.75f));

        GPUImageLevelsFilter levelsFilter = new GPUImageLevelsFilter();
        levelsFilter.setMin(0.0f, 3.0f, 1.0f);
        listGPUFilters.add(levelsFilter);

        listGPUFilters.add(new GPUImageHazeFilter());
        listGPUFilters.add(new GPUImageToonFilter());
        listGPUFilters.add(new GPUImageKuwaharaFilter());
    }

    private void setSelectedFrameThumb(View v) {
        FrameLayout frameLayout;
        for (int i = 0; i < llFrameList.getChildCount(); i++) {
            LinearLayout linearLayout = (LinearLayout) ((LinearLayout) llFrameList.getChildAt(i)).getChildAt(0);
            frameLayout = (FrameLayout) linearLayout.getChildAt(0);
            View view = frameLayout.getChildAt(1);

            if (v == linearLayout) {
                view.setVisibility(View.VISIBLE);
            } else {
                view.setVisibility(View.GONE);
            }
        }
    }

    private void setSelectedEffectThumb(View v) {
        FrameLayout frameLayout;
        for (int i = 0; i < llEffectList.getChildCount(); i++) {
            LinearLayout linearLayout = (LinearLayout) ((LinearLayout) llEffectList.getChildAt(i)).getChildAt(0);
            frameLayout = (FrameLayout) linearLayout.getChildAt(0);
            View view = frameLayout.getChildAt(1);

            if (v == linearLayout) {
                view.setVisibility(View.VISIBLE);
            } else {
                view.setVisibility(View.GONE);
            }
        }
    }

    private String getSelectedAudio() {
        String audioPath = "";

        if (isDefaultSongSelected) {
            audioPath = saveAudioRawToStorage();
        } else {
            try {
                audioPath = PathUtil.getPath(mContext, customSongUri);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }

        return audioPath;
    }

    private void setImageFrame(int id, View v) {
        if (id == 0) {
            if (selectedFrameIndex != 0) {
                selectedFrameIndex = 0;
                imgFrame.setImageResource(0);
                setSelectedFrameThumb(null);
            }
        } else {
            selectedFrameIndex = id;
            final ImageRequest imageRequest =
                    ImageRequestBuilder.newBuilderWithSource(Uri.parse(listFrames.get(id).path))
                            .setResizeOptions(new ResizeOptions(width, width))
                            .build();
            setSelectedFrameThumb(v);
            imgFrame.setImageRequest(imageRequest);
        }
    }

    private void applyImageDecoration(List<StickerView> listAddedStickers) {
        boolean isFrame, isFilter;
        isFrame = selectedFrameIndex != 0;
        isFilter = selectedFilterIndex != 0;

        try {
            for (int i = 0; i < listSelectedImagesPath.size(); i++) {
                BitmapUtil.bitmap = listSelectedImages.get(i);
                BitmapUtil.bitmap = createScaledImage(isFrame, isFilter, listAddedStickers);
                BitmapUtil.saveBitmap(BitmapUtil.bitmap, listSelectedImagesPath.get(i));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private Bitmap createScaledImage(boolean isFrame, boolean isFilter, List<StickerView> listAddedStickers) {
        Bitmap overlay = null;
        try {
            int width = frameLayout.getWidth();
            int height = frameLayout.getHeight();

            Item frame = listFrames.get(selectedFrameIndex);
            Paint paint = new Paint();
            paint.setColor(Color.WHITE);

            overlay = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(overlay);
            canvas.drawRect(0.0f, 0.0f, ScreenUtils.getScreenWidth(this), ScreenUtils.getScreenWidth(this),
                    paint);

            if ((!isFrame && !isFilter) || (isFrame && !isFilter)) {
                canvas.drawBitmap(Bitmap.createScaledBitmap(BitmapUtil.bitmap, width, height, false), 0, 0, null);
            }

            if (isFilter) {
                GPUImage gpuImage = new GPUImage(this);
                gpuImage.setFilter(listGPUFilters.get(selectedFilterIndex));
                gpuImage.setImage(BitmapUtil.bitmap);
                BitmapUtil.bitmap = gpuImage.getBitmapWithFilterApplied();
                canvas.drawBitmap(BitmapUtil.bitmap, 0.0f, 0.0f, null);
            }

            if (isFrame) {
                canvas.drawBitmap(Bitmap.createScaledBitmap(BitmapCompression.decodeSampledBitmapFromAssets(this, frame.path.replace("asset:///", ""),
                        width, height), width, height, false), 0.0f, 0.0f, null);
            }

            for (int i = 0; i < listAddedStickers.size(); i++) {
                StickerView stickerView = listAddedStickers.get(i);
                stickerView.setDrawingCacheEnabled(true);
                Bitmap b = Bitmap.createBitmap(stickerView.getDrawingCache());
                stickerView.setDrawingCacheEnabled(false);

                canvas.drawBitmap(b, stickerView.getX(), stickerView.getY(), null);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return overlay;
    }

    private void addStickerView(Bitmap sticker) {
        final StickerView stickerView = new StickerView(this);
        stickerView.setBitmap(sticker);

        stickerView.setOperationListener(new StickerView.OperationListener() {
            @Override
            public void onDeleteClick() {
                listAddedStickers.remove(stickerView);
                frameLayout.removeView(stickerView);
            }

            @Override
            public void onEdit(StickerView stickerView) {
                stickerCurrentEdit.setInEdit(false);
                stickerCurrentEdit = stickerView;
                stickerCurrentEdit.setInEdit(true);
            }

            @Override
            public void onTop(StickerView stickerView) {
                int position = listAddedStickers.indexOf(stickerView);
                if (position == listAddedStickers.size() - 1) {
                    return;
                }
                StickerView stickerTemp = listAddedStickers.remove(position);
                listAddedStickers.add(listAddedStickers.size(), stickerTemp);
            }
        });

        stickerView.setOnStickerTouchEvent(new StickerView.OnStickerTouch() {
            @Override
            public void actionStart() {
                scrollView.setScrollingEnabled(false);
            }

            @Override
            public void actionEnd() {
                scrollView.setScrollingEnabled(true);
            }
        });

        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        lp.gravity = Gravity.CENTER;
        frameLayout.addView(stickerView, lp);
        listAddedStickers.add(stickerView);
        setCurrentEdit(stickerView);
    }

    private void setCurrentEdit(StickerView stickerView) {
        if (stickerCurrentEdit != null) {
            stickerCurrentEdit.setInEdit(false);
        }

        stickerCurrentEdit = stickerView;
        stickerView.setInEdit(true);
    }

    private void unSelectStickerIfAdded() {
        for (StickerView stickerView : listAddedStickers) {
            stickerView.setInEdit(false);
        }
    }

    private void showFinalResultDialog() {

        View promptView = LayoutInflater.from(this).inflate(R.layout.layout_dialog_result, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptView);

        videoViewFinal = promptView.findViewById(R.id.videoView);
        playPauseButtonFinalVideo = promptView.findViewById(R.id.playPauseButtonFinalVideo);
        llShare = promptView.findViewById(R.id.llShare);

        videoViewFinal.setVideoURI(Uri.parse(destinationPath));
        videoViewFinal.seekTo(seek);


        playPauseButtonFinalVideo.setOnClickListener(this);
        llShare.setOnClickListener(this);


        videoViewFinal.setOnCompletionListener(mp -> {
            playPauseButtonFinalVideo.setToPlay();
            isPlayFinal.set(false);
            seek = 100;
        });


        dialogItem = alertDialogBuilder.create();
        dialogItem.setOnCancelListener(dialog -> {
            Intent intent = new Intent(this, SelectPhotoActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
            overridePendingTransition(0, 0);

        });
        dialogItem.show();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (dialogItem != null) {
            showFinalResultDialog();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (dialogItem != null) {
            dialogItem.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AdHandler.showFullScrenReloadAd();
        overridePendingTransitionExit();
    }

    private void setCallback() {
        Config.enableLogCallback(message -> durationToprogtess(message.getText()));
    }
}

