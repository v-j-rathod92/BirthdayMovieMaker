package com.lim.birthdaymoviemaker.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lim.birthdaymoviemaker.R;
import com.lim.birthdaymoviemaker.adapter.AudioListAdapter;
import com.lim.birthdaymoviemaker.util.SoundUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rgi-40 on 19/3/18.
 */

public class AudioListActivity extends BaseActivity {
    private Context mContext;
    private RecyclerView recyclerViewAudio;
    private List<Integer> listAudio;
    private AudioListAdapter adapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_audio_list);

        init();
    }

    private void init() {
        mContext = this;

        listAudio = new ArrayList<>();
        listAudio.add(R.raw.birthday1);
        listAudio.add(R.raw.birthday2);
        listAudio.add(R.raw.birthday3);

        recyclerViewAudio = findViewById(R.id.recyclerViewAudio);
        recyclerViewAudio.setLayoutManager(new LinearLayoutManager(mContext));

        adapter = new AudioListAdapter(this, listAudio);
        recyclerViewAudio.setAdapter(adapter);
    }

    public void onBirthdaySongSelect(int position) {
        Intent intent = new Intent();
        intent.putExtra(MakeMovieActivity.SELECTED_SONG, position);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SoundUtil.stop();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransitionExit();
    }
}
