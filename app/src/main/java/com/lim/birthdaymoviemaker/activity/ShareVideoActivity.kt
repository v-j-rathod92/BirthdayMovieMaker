package com.lim.birthdaymoviemaker.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.lim.birthdaymoviemaker.R
import com.lim.birthdaymoviemaker.databinding.ActivityShareVideoBinding
import com.lim.birthdaymoviemaker.helper.NativeAdHandler
import com.lim.birthdaymoviemaker.model.Share
import com.lim.birthdaymoviemaker.setupVideoPlayer
import com.lim.birthdaymoviemaker.util.SocialShare
import kotlinx.android.synthetic.main.layout_dialog_result.view.*
import kotlinx.android.synthetic.main.layout_toolbar.view.txtTitle
import java.io.File

class ShareVideoActivity : BaseActivity() {
    companion object {
        val VIDEO_PATH = "video_path"
    }

    private lateinit var binding: ActivityShareVideoBinding
    private lateinit var videoUri: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_share_video)

        init()
    }

    private fun init() {
        binding.toolbarInclude.txtTitle.text = getString(R.string.share_video)
        val imgHome = binding.toolbarInclude.imgHome
        imgHome.visibility = View.VISIBLE
        binding.shareList = SocialShare.prepareShareList(this)

        videoUri = intent.getStringExtra(VIDEO_PATH)

        binding.generatedImage = videoUri

        binding.flVideo.setupVideoPlayer(binding.uvv, binding.umc, videoUri)

        NativeAdHandler.loadNativeAd(this, binding.flNativeAd, getString(R.string.native_ad_id))

        imgHome.setOnClickListener {
            onBackPressed()
        }
    }

    fun onItemShared(share: Share) {
        val file = File(videoUri)

        when (share.id) {
            1 -> SocialShare.shareForAll(this, file)
            2, 3, 4, 5, 6 -> SocialShare.socialShare(this, file, share.pckg)
            7 -> SocialShare.sendMail(this, file)
        }
    }

    override fun onBackPressed() {
        Intent(this, SelectPhotoActivity::class.java)
                .apply {
                    startActivity(this)
                    finishAffinity()
                }
    }
}