package com.lim.birthdaymoviemaker.activity;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.lim.birthdaymoviemaker.R;
import com.lim.birthdaymoviemaker.adapter.AlbumAdapter;
import com.lim.birthdaymoviemaker.adapter.GridImageAdapter;
import com.lim.birthdaymoviemaker.custom.SpacesItemDecoration;
import com.lim.birthdaymoviemaker.helper.AdHandler;
import com.lim.birthdaymoviemaker.model.AlbumImageSelect;
import com.lim.birthdaymoviemaker.model.GalleryPhoto;
import com.lim.birthdaymoviemaker.model.GalleryPhotoList;
import com.lim.birthdaymoviemaker.model.SelectBucketImage;
import com.lim.birthdaymoviemaker.permission.GGPermissionManager;
import com.lim.birthdaymoviemaker.permission.OnRequestPermissionsCallBack;
import com.lim.birthdaymoviemaker.permission.PermissionRequest;
import com.lim.birthdaymoviemaker.util.BitmapCompression;
import com.lim.birthdaymoviemaker.util.PathUtil;
import com.lim.birthdaymoviemaker.util.ScreenUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class SelectPhotoActivity extends BaseActivity implements View.OnClickListener {

    private Context context;

    private Toolbar toolbar;
    private RecyclerView recyclerViewAlbums, recyclerViewGridImgs;
    private TextView txtSelected, txtTotal;
    private ImageView imgDelete;

    private ArrayList<GalleryPhotoList> arraylist = new ArrayList();
    private ArrayList<SelectBucketImage> listBucketImage = new ArrayList<>();
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    private AlbumAdapter albumAdapter;
    private GridImageAdapter gridImageAdapter;

    private HorizontalScrollView horizontalScrollView;
    private FrameLayout frameLayout;
    private LinearLayout linearLayoutSelectedImgs;
    private SimpleDraweeView simpleDraweeViewDummy = null;
    private ImageView imgRemove;
    private ProgressBar progressBar;

    public static int left = 15, top = 1598;
    private Button btnNext;
    private View selectedImageDummy;

    private List<Integer> listRemoveImageId = new ArrayList();
    private List<String> listRemoveImageUri = new ArrayList<>();
    private Handler handler = new Handler();
    private int id = 0;
    private boolean isImageSelectAnimationRunning = false;
    public static final String SELECTED_IMAGES = "selected_images;";
    private boolean isBackPressed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(this);
        setContentView(R.layout.layout_select_photo);

        init();
        setListener();
    }

    private void init() {
        context = this;
        AdHandler.getInstance(context);

        toolbar = findViewById(R.id.toolbar);
        horizontalScrollView = findViewById(R.id.horizontalScrollView);
        recyclerViewAlbums = findViewById(R.id.recyclerViewAlbums);
        recyclerViewGridImgs = findViewById(R.id.recyclerViewGridImgs);
        txtSelected = findViewById(R.id.txtSelected);
        txtTotal = findViewById(R.id.txtTotal);
        imgDelete = findViewById(R.id.imgDelete);
        frameLayout = findViewById(R.id.frameLayout);
        linearLayoutSelectedImgs = findViewById(R.id.linearLayoutSelectedImgs);
        btnNext = findViewById(R.id.btnNext);
        progressBar = findViewById(R.id.progressBar);

        ((TextView) toolbar.findViewById(R.id.txtTitle)).setText(getString(R.string.select_photo));
        setLayoutManager();
        initGridAdapter();
        addDummyPhoto();

        new GGPermissionManager.Builder(this)
                .addPermissions(PermissionRequest.readWriteStoragePermission()) //change permission as require
                .addRequestPermissionsCallBack(new OnRequestPermissionsCallBack() {
                    @Override
                    public void onGrant() {
                        loadImageList();
                    }

                    @Override
                    public void onDenied(String permission) {
                    }

                }).build().request();

        loadAds();
    }

    private void setListener() {
        imgDelete.setOnClickListener(this);
        btnNext.setOnClickListener(this);
    }

    private void loadAds() {
        AdHandler.loadFullScreenReloadAd(this);
    }

    private void setLayoutManager() {
        recyclerViewAlbums.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewGridImgs.setLayoutManager(new GridLayoutManager(this, 2));

        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.grid_space);
        recyclerViewGridImgs.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
    }

    private void initGridAdapter() {
        gridImageAdapter = new GridImageAdapter(new ArrayList<>(), context);
        recyclerViewGridImgs.setAdapter(gridImageAdapter);
    }

    private void loadImageList() {
        loadImageMedia().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ArrayList<GalleryPhoto>>() {
                    @Override
                    public void onSubscribe(Disposable disposable) {
                        compositeDisposable.add(disposable);
                    }

                    @Override
                    public void onNext(ArrayList<GalleryPhoto> galleryPhotos) {
                        setAlbumAndGalleryImage(galleryPhotos);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    private void setAlbumAndGalleryImage(ArrayList<GalleryPhoto> galleryPhotos) {
        for (int i = 0; i < galleryPhotos.size(); i++) {
            int size = listBucketImage.get(i).imgUri.size();
            arraylist.add(new GalleryPhotoList(galleryPhotos.get(i).bucketId,
                    galleryPhotos.get(i).bucketName,
                    galleryPhotos.get(i).count,
                    galleryPhotos.get(i).imgId,
                    galleryPhotos.get(i).imgUri, size, listBucketImage.get(i).imgUri));
        }


        albumAdapter = new AlbumAdapter(arraylist, context);
        recyclerViewAlbums.setAdapter(albumAdapter);
        gridImageAdapter.addAndNotify(listBucketImage.get(0).imgUri);
        txtTotal.setText("/" + arraylist.get(0).get_Folder_Size());
    }

    public void onAlbumSelect(GalleryPhotoList galleryPhotoList) {
        gridImageAdapter.addAndNotify(galleryPhotoList.getImgUris());
        txtTotal.setText("/" + galleryPhotoList.get_Folder_Size());
    }

    public void gridImageSelect(SimpleDraweeView selectedImage, ImageRequest imageRequest) {
        isImageSelectAnimationRunning = true;

        RelativeLayout.LayoutParams params;

        SimpleDraweeView simpleDraweeViewAnimating = new SimpleDraweeView(this);

        Rect rect = getViewGlobalVisibleRect(selectedImage);

        params = new RelativeLayout.LayoutParams(rect.width(), rect.height());
        simpleDraweeViewAnimating.setLayoutParams(params);

        simpleDraweeViewAnimating.setImageRequest(imageRequest);
        simpleDraweeViewAnimating.setX(rect.left);
        simpleDraweeViewAnimating.setY(rect.top - 70);

        frameLayout.addView(simpleDraweeViewAnimating);

        Rect rectDummy = getViewGlobalVisibleRect(selectedImageDummy);

        simpleDraweeViewAnimating.animate().x(rectDummy.left + getResources().getDimensionPixelSize(R.dimen.margin_selected_image))
                .y(SelectPhotoActivity.top - 70).setDuration(300).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                frameLayout.removeView(simpleDraweeViewAnimating);

                simpleDraweeViewDummy = selectedImageDummy.findViewById(R.id.simpleDraweeViewDummy);
                simpleDraweeViewDummy.setImageRequest(imageRequest);
                selectedImageDummy.setVisibility(View.VISIBLE);

                imgRemove = selectedImageDummy.findViewById(R.id.imgRemove);
                imgRemove.setId(id);
                listRemoveImageId.add(id);
                listRemoveImageUri.add(imageRequest.getSourceUri().toString());

                id += 1;

                imgRemove.setOnClickListener(SelectPhotoActivity.this);
//                imgRemove.postDelayed(() -> imgRemove.setVisibility(View.VISIBLE), 100);

                selectedImageDummy = getSelectedImageView();
                linearLayoutSelectedImgs.addView(selectedImageDummy);

                setTextSelectedImage();
                animateRemoveImage();

                handler.postDelayed(() -> isImageSelectAnimationRunning = false, 200);


            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).start();
    }

    @Override
    protected void onStop() {
        super.onStop();

        compositeDisposable.clear();
    }

    private Observable<ArrayList<GalleryPhoto>> loadImageMedia() {
        String lastBucketID;
        SelectBucketImage selection;
        ArrayList<AlbumImageSelect> albumData;
        ArrayList<GalleryPhoto> data = new ArrayList<>();

        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{"_id", "_data", "bucket_id", "bucket_display_name", "width", "height"};
        ArrayList<String> ids = new ArrayList();

        Cursor cursor = getContentResolver().query(uri, projection, "bucket_display_name != ?",
                new String[]{getString(R.string.app_name)}, "bucket_display_name ASC,_id DESC");

        if (cursor.getCount() >= 0 && cursor.moveToFirst()) {
            int bucketColumn = cursor.getColumnIndex("bucket_display_name");
            int idColumn = cursor.getColumnIndex("bucket_id");
            int mediaColumn = cursor.getColumnIndex("_id");

            lastBucketID = cursor.getString(idColumn);
            selection = new SelectBucketImage();
            albumData = new ArrayList<>();
            selection.bucketid = lastBucketID;

            do {
                GalleryPhoto gallaryAlbum = new GalleryPhoto();
                gallaryAlbum.bucketName = cursor.getString(bucketColumn);
                gallaryAlbum.bucketId = cursor.getString(idColumn);
                int imageID = cursor.getInt(mediaColumn);
                Uri imgUri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, Integer.toString(imageID));

                if (new File(cursor.getString(cursor.getColumnIndex("_data"))).exists()) {

                    int width = 0, height = 0;
                    try {
                        width = Integer.parseInt(cursor.getString(cursor.getColumnIndex("width")));
                        height = Integer.parseInt(cursor.getString(cursor.getColumnIndex("height")));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    if (!ids.contains(gallaryAlbum.bucketId)) {
                        ids.add(gallaryAlbum.bucketId);
                        gallaryAlbum.imgUri = imgUri;
                        gallaryAlbum.imgId = imageID;
                        data.add(gallaryAlbum);

                        if (!lastBucketID.equals(gallaryAlbum.bucketId)) {
                            selection.bucketid = lastBucketID;
                            selection.imgUri = new ArrayList();
                            selection.imgUri.addAll(albumData);

                            listBucketImage.add(selection);

                            lastBucketID = gallaryAlbum.bucketId;
                            selection = new SelectBucketImage();
                            albumData = new ArrayList();
                        }
                    }

                    AlbumImageSelect albumImg = new AlbumImageSelect();
                    albumImg.imgUri = imgUri;
                    albumImg.imgId = Integer.valueOf(imageID);
                    albumImg.imgPos = -1;
                    albumImg.width = width;
                    albumImg.height = height;
                    albumData.add(albumImg);
                }
            } while (cursor.moveToNext());

            selection.bucketid = lastBucketID;
            selection.imgUri = new ArrayList();
            selection.imgUri.addAll(albumData);
            listBucketImage.add(selection);
        }

        return Observable.just(data);
    }

    public View getSelectedImageView() {
        View view = LayoutInflater.from(this).inflate(R.layout.layout_selected_image, null);
        return view;
    }

    private void addDummyPhoto() {
        selectedImageDummy = getSelectedImageView();
        linearLayoutSelectedImgs.addView(selectedImageDummy);
        imgRemove = selectedImageDummy.findViewById(R.id.imgRemove);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void animateRemoveImage() {
        ImageView img = new ImageView(this);
        int size = getResources().getDimensionPixelSize(R.dimen.img_size_remove);
        ViewGroup.LayoutParams param = new ViewGroup.LayoutParams(size, size);
        img.setLayoutParams(param);
        img.setBackground(getResources().getDrawable(R.drawable.shape_remove_photo));
        img.setImageResource(R.drawable.ic_clear);
        int padding = getResources().getDimensionPixelSize(R.dimen.dp_3);
        img.setPadding(padding, padding, padding, padding);
        img.setVisibility(View.INVISIBLE);
        frameLayout.addView(img);

        Rect rectSelectedImagesLayout = getViewGlobalVisibleRect(linearLayoutSelectedImgs);
        Rect rect = getViewGlobalVisibleRect(imgRemove);

        Integer[] animateCoordinate = getAnimateCoordinate();

        img.setX(animateCoordinate[0]);
        img.setY(animateCoordinate[1]);

        if (rect.right > rectSelectedImagesLayout.width()) {

            horizontalScrollView.post(() -> horizontalScrollView.fullScroll(HorizontalScrollView.FOCUS_RIGHT));

            handler.postDelayed(() -> {
                Rect rectTemp = getViewGlobalVisibleRect(imgRemove);
                img.setVisibility(View.VISIBLE);
                playRemoveImgAnimation(img, rectTemp, true);
            }, 200);
        } else {
            img.setVisibility(View.VISIBLE);
            playRemoveImgAnimation(img, rect, false);
        }


    }

    private void playRemoveImgAnimation(ImageView img, Rect rect, boolean isOut) {
        isImageSelectAnimationRunning = true;

        img.animate().x(rect.left).y(rect.top - rect.height() - 10).setDuration(400)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        frameLayout.removeView(img);
                        imgRemove.setVisibility(View.VISIBLE);

                        if (!isOut) {
                            horizontalScrollView.post(() -> horizontalScrollView.fullScroll(HorizontalScrollView.FOCUS_RIGHT));
                        }

                        isImageSelectAnimationRunning = false;
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .start();
    }

    @Override
    public void onClick(View v) {
        boolean isInProgress = progressBar.getVisibility() == View.VISIBLE;

        if (v.getId() == R.id.imgDelete) {
            if (isInProgress) {
                showProgressMsg();
            } else {
                deleteAllSelectedImages();
            }
        } else if (v.getId() == R.id.btnNext) {

            if (isInProgress) {
                showProgressMsg();
            } else {
                try {
                    if (listRemoveImageUri.size() > 1) {
                        new AsyncTaskResizeImage().execute();
                    } else {
                        Toast.makeText(context, "Please select at least two image", Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    progressBar.setVisibility(View.GONE);
                    e.printStackTrace();
                }
            }
        } else {
            try {
                linearLayoutSelectedImgs.removeViewAt(listRemoveImageId.indexOf(v.getId()));
                gridImageAdapter.removeSelectedImage(listRemoveImageUri.get(listRemoveImageId.indexOf(v.getId())));
                listRemoveImageUri.remove(listRemoveImageId.indexOf(v.getId()));
                listRemoveImageId.remove(listRemoveImageId.indexOf(v.getId()));
                setTextSelectedImage();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void showProgressMsg() {
        Toast.makeText(this, "Processing....", Toast.LENGTH_LONG).show();
    }

    class AsyncTaskResizeImage extends AsyncTask<Void, Void, List<String>> {
        boolean isCurrepted = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<String> doInBackground(Void... voids) {
            List<String> listSelectedImages = new ArrayList<>();

            try {
                for (int i = 0; i < listRemoveImageUri.size(); i++) {
                    int width = ScreenUtils.getScreenWidth(context);
                    String path = PathUtil.getPath(context, Uri.parse(listRemoveImageUri.get(i)));
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    options.inSampleSize = BitmapCompression.calculateInSampleSize(options, width, width);
                    BitmapFactory.decodeFile(path, options);
                    options.inJustDecodeBounds = false;
                    Bitmap bitmap = BitmapCompression.adjustImageOrientation(new File(path), BitmapFactory.decodeFile(path, options));
                    bitmap = BitmapCompression.scaleCenterCrop(bitmap, width, width); // can be remove

                    String imgPath = PathUtil.getFilename(context, i);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 70, new FileOutputStream(imgPath));

                    listSelectedImages.add(imgPath);
                }

            } catch (Exception e) {
                e.printStackTrace();
                isCurrepted = true;
                listSelectedImages.clear();
            }

            return listSelectedImages;
        }

        @Override
        protected void onPostExecute(List<String> list) {
            super.onPostExecute(list);

            progressBar.setVisibility(View.GONE);

            if (isCurrepted) {
                Toast.makeText(context, "Sorry...may be you've added corrupted image", Toast.LENGTH_LONG).show();
            } else {
                Intent intent = new Intent(context, MakeMovieActivity.class);
                intent.putStringArrayListExtra(SELECTED_IMAGES, (ArrayList<String>) list);
                startActivity(intent);
                overridePendingTransitionEnter();
                AdHandler.showFullScrenReloadAd();
            }
        }
    }


    private void deleteAllSelectedImages() {
        listRemoveImageId.clear();
        listRemoveImageUri.clear();
        linearLayoutSelectedImgs.removeAllViews();
        gridImageAdapter.clearSelectedImage();
        id = 0;
        addDummyPhoto();
        setTextSelectedImage();
    }

    private void setTextSelectedImage() {
        txtSelected.setText(String.valueOf(linearLayoutSelectedImgs.getChildCount() - 1));
    }

    private Integer[] getAnimateCoordinate() {
        int random = new Random().nextInt(3);
        Rect rect = getViewGlobalVisibleRect(linearLayoutSelectedImgs);
        Integer coordinate[] = new Integer[2];

        int limitHeight = rect.top;
        int limitWidth = ScreenUtils.getScreenWidth(this);

        if (random == 0) {
            coordinate[0] = 0;
            coordinate[1] = new Random().nextInt(limitHeight);
        } else if (random == 1) {
            coordinate[0] = new Random().nextInt(limitWidth);
            coordinate[1] = 0;
        } else if (random == 2) {
            coordinate[0] = limitWidth;
            coordinate[1] = new Random().nextInt(limitHeight);
        }

        return coordinate;
    }


    private Rect getViewGlobalVisibleRect(View view) {
        Rect rect = new Rect();
        view.getGlobalVisibleRect(rect);
        return rect;
    }

    public boolean isImageSelectAnimationRunning() {
        return isImageSelectAnimationRunning;
    }

    @Override
    public void onBackPressed() {
        if (isBackPressed) {
            super.onBackPressed();
            overridePendingTransitionExit();
        } else {
            if (AdHandler.isFullScreenReloadLoaded()) {
                isBackPressed = true;
                AdHandler.showFullScrenReloadAd();
            } else {
                super.onBackPressed();
                overridePendingTransitionExit();
            }
        }
    }
}

