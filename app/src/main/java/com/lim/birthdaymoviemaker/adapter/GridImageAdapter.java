package com.lim.birthdaymoviemaker.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.common.RotationOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.lim.birthdaymoviemaker.R;
import com.lim.birthdaymoviemaker.activity.SelectPhotoActivity;
import com.lim.birthdaymoviemaker.model.AlbumImageSelect;
import com.lim.birthdaymoviemaker.util.ScreenUtils;

import java.util.ArrayList;

/**
 * Created by Vishal Rathod
 */
public class GridImageAdapter extends RecyclerView.Adapter<GridImageAdapter.ViewHolder> {

    public ArrayList<AlbumImageSelect> imgUri;
    private Context context;
    private int size;
    private ArrayList<String> selectedImages = new ArrayList<>();

    public GridImageAdapter(ArrayList<AlbumImageSelect> imgUri, Context context) {
        this.imgUri = imgUri;
        this.context = context;
        size = ScreenUtils.convertDIPToPixels(context, 90);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_gridimage, null);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (selectedImages.contains(imgUri.get(position).imgUri.toString())) {
            holder.relativeLayoutMask.setVisibility(View.VISIBLE);
        } else {
            holder.relativeLayoutMask.setVisibility(View.GONE);
        }


        final ImageRequest imageRequest =
                ImageRequestBuilder.newBuilderWithSource(imgUri.get(position).imgUri)
                        .setResizeOptions(new ResizeOptions(size, size))
                        .setRotationOptions(RotationOptions.autoRotate())
                        .build();

        holder.simpleDraweeViewImage.setImageRequest(imageRequest);

        holder.relativeLayoutRoot.setOnClickListener(v -> {
            if (!((SelectPhotoActivity) context).isImageSelectAnimationRunning() &&
                    !selectedImages.contains(imgUri.get(position).imgUri.toString())) {
                selectedImages.add(imgUri.get(position).imgUri.toString());
                holder.relativeLayoutMask.setVisibility(View.VISIBLE);
                ((SelectPhotoActivity) context).gridImageSelect(holder.simpleDraweeViewImage, imageRequest);
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void addAndNotify(ArrayList<AlbumImageSelect> imgUri) {
        if (this.imgUri != null) {
            this.imgUri.clear();
            this.imgUri.addAll(imgUri);
            notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        return imgUri.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeLayoutRoot, relativeLayoutMask;
        SimpleDraweeView simpleDraweeViewImage;

        public ViewHolder(View itemView) {
            super(itemView);

            relativeLayoutRoot = itemView.findViewById(R.id.relativeLayoutRoot);
            relativeLayoutMask = itemView.findViewById(R.id.relativeLayoutMask);
            simpleDraweeViewImage = itemView.findViewById(R.id.simpleDraweeViewImage);
        }
    }

    public void removeSelectedImage(String uri) {
        selectedImages.remove(uri);
        notifyDataSetChanged();
    }

    public void clearSelectedImage() {
        selectedImages.clear();
        notifyDataSetChanged();
    }

}
