package com.lim.birthdaymoviemaker.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lim.birthdaymoviemaker.activity.ShareVideoActivity
import com.lim.birthdaymoviemaker.callback.OnShareItemClick
import com.lim.birthdaymoviemaker.databinding.ItemShareBinding
import com.lim.birthdaymoviemaker.model.Share

class ShareAdapter(val shareList: List<Share>, val context: Context) :
        RecyclerView.Adapter<ShareAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemShareBinding.inflate(
                LayoutInflater.from(parent.context),
                parent, false
        )

        return ViewHolder(binding, context)
    }

    override fun getItemCount(): Int {
        return shareList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(shareList[position])
    }

    class ViewHolder(
            val binding: ItemShareBinding,
            context: Context
    ) : RecyclerView.ViewHolder(binding.root) {
        private val onShareItemClick = object : OnShareItemClick {
            override fun onItemClick(share: Share) {
                (context as ShareVideoActivity).onItemShared(share)
            }
        }

        fun bind(share: Share) {
            binding.share = share
        }

        init {
            binding.onShareItemClick = onShareItemClick
        }
    }
}