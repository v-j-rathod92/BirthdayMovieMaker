package com.lim.birthdaymoviemaker.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.common.RotationOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.lim.birthdaymoviemaker.R;
import com.lim.birthdaymoviemaker.activity.SelectPhotoActivity;
import com.lim.birthdaymoviemaker.model.GalleryPhotoList;
import com.lim.birthdaymoviemaker.util.ScreenUtils;

import java.util.ArrayList;

/**
 * Created by Vishal Rathod
 */
public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.ViewHolder> {


    private ArrayList<GalleryPhotoList> galleryPhotoLists;
    private Context context;
    private int size;

    public AlbumAdapter(ArrayList<GalleryPhotoList> galleryPhotoLists, Context context) {
        this.galleryPhotoLists = galleryPhotoLists;
        this.context = context;
        size = ScreenUtils.convertDIPToPixels(context, 80);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_row_album, null);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
//        holder.simpleDrawViewAlbum.setImageURI(gallaryPhotos.get(position).imgUri);

//        SimpleDraweeView view = (SimpleDraweeView) holder.simpleDrawViewAlbum;
//
//
//        DraweeController ctrl = Fresco.newDraweeControllerBuilder()
//                .setUri(gallaryPhotos.get(position).imgUri)
//                .setOldController(view.getController())
//                .setAutoPlayAnimations(true)
//                .build();
//
//        Fresco.getImagePipeline().prefetchToBitmapCache(
//                ImageRequest.fromUri(gallaryPhotos.get(position).imgUri), view.getContext());
//        view.setController(ctrl);


        final ImageRequest imageRequest =
                ImageRequestBuilder.newBuilderWithSource(galleryPhotoLists.get(position).getFile_imgUri())
                        .setResizeOptions(new ResizeOptions(size, size))
                        .setRotationOptions(RotationOptions.autoRotate())
                        .build();
        holder.simpleDrawViewAlbum.setImageRequest(imageRequest);

        holder.txtAlbumName.setText(galleryPhotoLists.get(position).getFile_bucketName() + "\n(" +
                galleryPhotoLists.get(position).get_Folder_Size() + ")");

        holder.linearLayoutRoot.setOnClickListener(v ->
                ((SelectPhotoActivity) context).onAlbumSelect(galleryPhotoLists.get(position)));
    }

    @Override
    public int getItemCount() {
        return galleryPhotoLists.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout linearLayoutRoot;
        SimpleDraweeView simpleDrawViewAlbum;
        TextView txtAlbumName;

        public ViewHolder(View itemView) {
            super(itemView);

            linearLayoutRoot = itemView.findViewById(R.id.linearLayoutRoot);
            simpleDrawViewAlbum = itemView.findViewById(R.id.simpleDrawViewAlbum);
            txtAlbumName = itemView.findViewById(R.id.txtAlbumName);
        }
    }
}
