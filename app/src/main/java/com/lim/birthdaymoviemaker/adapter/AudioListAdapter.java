package com.lim.birthdaymoviemaker.adapter;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lim.birthdaymoviemaker.R;
import com.lim.birthdaymoviemaker.activity.AudioListActivity;
import com.lim.birthdaymoviemaker.custom.MaterialPlayPauseButton;
import com.lim.birthdaymoviemaker.databinding.LayoutAudioRowBinding;
import com.lim.birthdaymoviemaker.util.SoundUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by rgi-40 on 20/3/18.
 */

public class AudioListAdapter extends RecyclerView.Adapter<AudioListAdapter.ViewHolder> {

    private Context mContext;
    private List<Integer> listAudio;
    private List<String> listName = new ArrayList<>(Arrays.asList("Birthday 1", "Birthday 2", "Birthday 3"));
    private int currentPlayIndex = -1;

    public AudioListAdapter(Context mContext, List<Integer> listAudio) {
        this.mContext = mContext;
        this.listAudio = listAudio;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutAudioRowBinding binding = DataBindingUtil.inflate(LayoutInflater.from(mContext),
                R.layout.layout_audio_row, parent, false);
        binding.setNames(listName);
        binding.setAdapter(this);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.bindVar.setPosition(position);

        if (currentPlayIndex != -1 && position == currentPlayIndex) {
            holder.bindVar.playPauseButton.setToPause();
        } else {
            holder.bindVar.playPauseButton.setToPlay();
        }
    }

    @Override
    public int getItemCount() {
        return listAudio.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LayoutAudioRowBinding bindVar;

        public ViewHolder(LayoutAudioRowBinding itemView) {
            super(itemView.getRoot());
            bindVar = itemView;

        }
    }

    public void onPlayPauseClick(View v, int position) {

        MaterialPlayPauseButton playPauseButton = (MaterialPlayPauseButton) v;
        if (playPauseButton.getState() == 0) {
            currentPlayIndex = position;
            playPauseButton.setToPause();
            SoundUtil.play(mContext, listAudio.get(position));
        } else {
            currentPlayIndex = -1;
            playPauseButton.setToPlay();
            SoundUtil.stop();
        }

        notifyDataSetChanged();
    }

    public void onBirthdaySongSelect(int position) {
        ((AudioListActivity) mContext).onBirthdaySongSelect(position);
    }
}
