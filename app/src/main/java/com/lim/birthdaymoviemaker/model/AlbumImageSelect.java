package com.lim.birthdaymoviemaker.model;

import android.net.Uri;

/**
 * Created by Vishal Rathod
 */
public class AlbumImageSelect {
    public int height;
    public Integer imgId;
    public int imgPos = -1;
    public Uri imgUri;
    public int width;
}
