package com.lim.birthdaymoviemaker.model;

import android.net.Uri;

/**
 * Created by Vishal Rathod
 */
public class GalleryPhoto {
    public String bucketId;
    public String bucketName;
    public int count;
    public int imgId;
    public Uri imgUri;
}
