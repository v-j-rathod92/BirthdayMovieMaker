package com.lim.birthdaymoviemaker.model;

/**
 * Created by Vishal Rathod
 */
public class Item {
    public boolean isAvailable;
    public String path;

    public Item(String path, boolean isAvail) {
        this.path = path;
        this.isAvailable = isAvail;
    }
}

