package com.lim.birthdaymoviemaker.model

import android.graphics.drawable.Drawable

data class Share(val id: Int, val drawable: Drawable?, val name: String, val pckg: String)