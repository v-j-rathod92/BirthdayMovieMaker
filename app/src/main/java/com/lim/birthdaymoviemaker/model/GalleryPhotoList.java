package com.lim.birthdaymoviemaker.model;

import android.net.Uri;

import java.util.ArrayList;

public class GalleryPhotoList {
    private int folder_Size;
    private String bucketId;
    private String bucketName;
    private int count;
    private int imgId;
    private Uri imgUri;
    public ArrayList<AlbumImageSelect> imgUris;

    public GalleryPhotoList(String bucketId, String bucketName, int count, int imgId, Uri imgUri, int Folder_Size, ArrayList<AlbumImageSelect> imgUris) {
        this.bucketId = bucketId;
        this.bucketName = bucketName;
        this.count = count;
        this.imgId = imgId;
        this.imgUri = imgUri;
        this.folder_Size = Folder_Size;
        this.imgUris = imgUris;
    }

    public String getbucketId() {
        return this.bucketId;
    }

    public String getFile_bucketName() {
        return this.bucketName;
    }

    public int getFile_count() {
        return this.count;
    }

    public int getFile_imgId() {
        return this.imgId;
    }

    public Uri getFile_imgUri() {
        return this.imgUri;
    }

    public int get_Folder_Size() {
        return this.folder_Size;
    }

    public ArrayList<AlbumImageSelect> getImgUris() {
        return imgUris;
    }

}
