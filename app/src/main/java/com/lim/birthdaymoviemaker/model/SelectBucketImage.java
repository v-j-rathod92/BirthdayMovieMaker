package com.lim.birthdaymoviemaker.model;

/**
 * Created by Vishal Rathod
 */
import java.util.ArrayList;

public class SelectBucketImage {
    public String bucketid;
    public int count;
    public ArrayList<AlbumImageSelect> imgUri;
}
